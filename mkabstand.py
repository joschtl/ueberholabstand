#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import sys

# Konstanten
bildbreite = 1920
bildhoehe = 1080
markierung = 12  # 15 auf Autobahnen, 12 sonst
scale = 2

infotext = """Quellen:
¹) Defaultwert nach Messung von @natenom an der Landstraße L574,
   https://twitter.com/Natenom/status/1286643200208384000
   Er misst 3,20 zwischen den Linien, laut ⁷) ist eine Linie 12 cm breit.
²) Mein Lenker ist 65 cm breit. Ob meine Ellbogen oder so weiter rausstehen,
   weiß ich nicht...
³) aus einem Schriftwechsel mit @natenom, siehe
   https://blog.natenom.com/2020/07/bussgeldstelle-pforzheim-berechnet-abstaende-ab-mitte-des-radfahrenden
   Zitat:
     \"außerorts (z.B. bei einer durchschnittlichen Fahrbahnbreite von 3,75 m):
      Abstand zur weißen Linie rechts: 0,75 m
      Den Abstand nach rechts rechne ich ab den Rädern, es ist nicht nachzuvollziehen,
      hierfür die rechte Außenkante des Lenkers heranzuziehen.\"
   Lediglich innerorts neben geparkten Autos berechnet die Bußgeldstelle Pforzheim den Seitenabstand
   von in diesem Fall 1,20 m ab dem rechten Lenkerende.
   Daraus resultiert diese Aussage:
     \"Wenn ein Fahrzeug mittig auf der Mittellinie überholt, liegt ein geringfügiger Verstoß vor.
      Bei einer durchschnittlichen Fahrzeugbreite von 1,80 m ist es 0,90 m auf der rechten Fahrbahnseite.\"
⁴) https://assets.adac.de/image/upload/v1573473524/ADAC-eV/KOR/Text/PDF/Fahrzeugbreiten_hgw9tn.pdf
⁵) https://de.wikipedia.org/wiki/Nutzfahrzeug/Ma%C3%9Fe_und_Gewichte
⁶) ich habe auf Twitter gefragt, wie weit Außenspiegel rausstehen, weil ich keine Quelle finden
   konnte. @schlabonski war so freundlich, mir zu antworten: ca. 25 cm rechts und links
   (https://twitter.com/schlabonski/status/1287010147756367873). Damit wäre die Gesamtbreite mit
   Spiegeln 305 cm. Da es aber Straßen geben darf, die nur je 300 cm breite Fahrspuren haben (siehe
   ⁷)), habe ich das eigenmächtig auf 300 cm reduziert.
⁷) früher https://de.wikipedia.org/wiki/Richtlinien_f%C3%BCr_die_Anlage_von_Stra%C3%9Fen_%E2%80%93_Querschnitt,
   aktuell https://www.bast.de/BASt_2017/DE/Verkehrstechnik/Fachthemen/v1-strassentypen.html
⁸) https://de.wikipedia.org/wiki/Bildtafel_der_Verkehrszeichen_in_der_Bundesrepublik_Deutschland_seit_2017,
   Zeichen 277.1. Von hier habe ich auch die Auto-Grafik geholt, selbst um Rückspiegel erweitert.
   und in Höhe und Breite so verzerrt, dass das Auto- und das SUV-Bild entstanden.
   Das LKW-Bild stammt vom Zeichen 277 auf der selben Seite, Spiegel ebenfalls von mir.
⁹) Das Bild der radfahrenden Person hat mir @lebenswerteCity zur Verfügung gestellt - es ist das SVG, das
   https://twitter.com/lebenswerteCity/status/1290250052703645698 zugrunde liegt.

Mein Dank gilt @natenom, @schlabonski und @lebenswerteCity, die mir mit Antworten, Nachdenkhilfen, Ansporn
und grafischen Fähigkeiten geholfen haben.

Es gibt noch massenweise Möglichkeiten, was zu verbessern - nur zu, wenn ihr wollt und könnt - oder schreibt
mir Anregungen auf Twitter an @joschtl oder per Mail an radwrdlabstand.20.pcb@a-bc.net.
"""


def create_convert_command(args):
    fahrstreifen = args.fahrstreifen
    fahrradbreite = args.fahrradbreite
    typ = args.typ
    mittelstreifen = args.mittelstreifen
    autoabstand = args.autoabstand
    stvo = args.stvo
    fahrradabstand = args.fahrradabstand
    seitenstreifen = args.seitenstreifen
    mensch = args.mensch

    filename = "Abstand_" + str(fahrstreifen) + "_" + str(fahrradbreite) + "_"

    # Berechnetes
    fahrradbreitepx = scale * fahrradbreite

    if typ == 2:
        autobreite = 300
        autobreitepx = scale * autobreite
        # 2,55 m ohne, 3,00 mit Spiegeln, 4 m hoch
        autohoehepx = autobreitepx * 4000 / 3000
        autobildschwarz = "lkw_mit.png"
        autobildrot = "lkw_mit_rot.png"
        filename += "LKW_"
    elif typ == 1:
        autobreite = 222
        autobreitepx = scale * autobreite
        # Audi Q7: 1,97 m ohne, 2,22 m mit Spiegeln, 1,74 hoch
        autohoehepx = autobreitepx * 1740 / 2220
        autobildschwarz = "suv_mit.png"
        autobildrot = "suv_mit_rot.png"
        filename += "SUV_"
    else:
        autobreite = 203
        autobreitepx = scale * autobreite
        # Mini Clubman: 1,80 m ohne, 2,03 m mit Spiegeln, 1,441 m hoch
        autohoehepx = autobreitepx * 1441 / 2030
        autobildschwarz = "auto_mit.png"
        autobildrot = "auto_mit_rot.png"
        filename += "Auto_"

    seitenstreifenpx = scale * seitenstreifen
    markierungpx = scale * markierung
    fahrstreifenpx = scale * fahrstreifen
    mitte = bildbreite / 2
    fahrradabstandpx = scale * fahrradabstand
    autoabstandpx = scale * autoabstand

    if mittelstreifen == 1:
        asphaltlinks = mitte - fahrstreifenpx - seitenstreifenpx
        asphaltrechts = mitte + fahrstreifenpx + seitenstreifenpx
        filename = filename + "mit_" + str(autoabstand) + "_"
    else:
        asphaltlinks = mitte - fahrstreifenpx/2 - seitenstreifenpx
        asphaltrechts = mitte + fahrstreifenpx/2 + seitenstreifenpx
        filename = filename + "ohne_" + str(autoabstand) + "_"

    linkslinks = asphaltlinks + seitenstreifenpx - markierungpx
    linksrechts = asphaltlinks + seitenstreifenpx
    rechtslinks = asphaltrechts - seitenstreifenpx
    rechtsrechts = asphaltrechts - seitenstreifenpx + markierungpx
    mittelinks = mitte - markierungpx/2
    mitterechts = mitte + markierungpx/2

    if stvo == 0:
        fahrradmitte = rechtslinks - fahrradabstandpx
        fahrradlinks = fahrradmitte - fahrradbreitepx/2
        fahrradrechts = fahrradmitte + fahrradbreitepx/2
        filename = filename + "PF_" + \
            str(fahrradabstand) + "_" + str(seitenstreifen) + "_"
    else:
        fahrradrechts = rechtslinks - fahrradabstandpx
        fahrradmitte = fahrradrechts - fahrradbreitepx/2
        fahrradlinks = fahrradmitte - fahrradbreitepx/2
        filename = filename + "StVO_" + \
            str(fahrradabstand) + "_" + str(seitenstreifen) + "_"

    if mensch == 0:
        fahrradhoehepx = scale * 122
        fahrradbild = "fahrrad.png"
        filename += "Rad"
    else:
        fahrradhoehepx = scale*183
        fahrradbild = "radler.png"
        filename += "RadlerIn"
    filename += ".png"

    fahrradoben = 900 - fahrradhoehepx
    autolinks = fahrradlinks - autoabstandpx - autobreitepx
    autorechts = fahrradlinks - autoabstandpx

    if autolinks < linksrechts:
        autolinks = linksrechts
        autorechts = autolinks + autobreitepx
        autoschwarz = 0
        autoabstandpx = fahrradlinks - autorechts
        autoabstand = (int) (autoabstandpx / scale)
    else:
        autoschwarz = 1

    autooben = 900 - autohoehepx
    seitenstreifentextlinks = asphaltlinks + seitenstreifenpx/2-8
    seitenstreifentextrechts = asphaltrechts - seitenstreifenpx/2-8
    fahrstreifentextlinks = asphaltlinks + seitenstreifenpx + fahrstreifenpx/2-8
    fahrstreifentextrechts = asphaltrechts - seitenstreifenpx - fahrstreifenpx/2-8
    autoabstandtext = (fahrradlinks + autorechts)/2-8

    if stvo == 0:
        fahrradabstandtext = (rechtslinks + fahrradmitte)/2-8
    else:
        fahrradabstandtext = (rechtslinks + fahrradrechts)/2-8

    # Erklärtexte
    line1 = "Fahrstreifenbreite: " + str(fahrstreifen) + " cm"
    line2 = "Autobreite mit Spiegel: " + str(autobreite) + " cm"

    if typ == 2:
        line2 = "LKW mit Maximalmaßen: Breite 2,55 m ohne, 3,00 m mit Spiegeln; Höhe 4,00 m"
    elif typ == 1:
        line2 = "typischer SUV: Audi Q7. Breite 1,968 m ohne, 2,22 m mit Spiegeln; Höhe 1,74 m"
    else:
        line2 = "typisches Auto: Mini Clubman. Breite 1,80 m ohne, 2,03 m mit Spiegeln; Höhe 1,441 m"
        line3 = "(ausgewählt wegen der Aussage der Bußgeldstelle Pforzheim, dass ein Auto 1,80 m breit sei)"

    line3 = "Überholabstand (Spiegel zu linkem Lenkerende): " + \
        str(autoabstand) + " cm"
    line4 = "Fahrradbreite: " + str(fahrradbreite) + " cm"
    if stvo == 0:
        line5 = "Abstand Fahrradmitte zum linken Rand der Fahrbahnbegrenzungsmarkierung: " + \
        str(fahrradabstand) + " cm"
    else:
        line5 = "Abstand rechtes Lenkerende zum linken Rand der Fahrbahnbegrenzungsmarkierung: " + \
        str(fahrradabstand) + " cm"
    line6 = "https://gitlab.com/joschtl/ueberholabstand"

    # Bilderzeugung
    cs = "convert -size " + str(bildbreite) + "x" + str(bildhoehe)
    cs += " canvas:darkgray "
    cs += "-fill black -strokewidth 0 -stroke none "
    cs += "-pointsize 60 "
    cs += "-annotate +770+70 \"Überholabstand\" "
    cs += "-pointsize 18 "
    cs += "-annotate +1000+120 \"" + line1 + "\" "
    cs += "-annotate +1000+140 \"" + line2 + "\" "
    cs += "-annotate +1000+160 \"" + line3 + "\" "
    cs += "-annotate +1000+180 \"" + line4 + "\" "
    cs += "-annotate +1000+200 \"" + line5 + "\" "
    if not args.nourl:
        cs += "-pointsize 12 "
        cs += "-annotate +1680+1070 \"" + line6 + "\" "
    cs += "-stroke black -fill black -strokewidth 0 "
    cs += "-draw \"rectangle " + \
        str(asphaltlinks) + ",900," + str(asphaltrechts) + ",920\" "
    cs += "-stroke white -fill white "

    if mittelstreifen == 1:
        cs += "-draw \"rectangle " + \
            str(mittelinks) + ",895," + str(mitterechts) + ",910\" "

    if seitenstreifen > 0:
        cs += "-draw \"rectangle {},895,{},910\" ".format(
            linkslinks, linksrechts)
        cs += "-draw \"rectangle {},895,{},910\" ".format(
            rechtslinks, rechtsrechts)
    else:
        cs += "-stroke '#505050' -fill '#505050' "
        cs += "-draw \"rectangle \"{}\",920 \"{}\",860\" ".format(
            asphaltlinks-20, asphaltlinks-50)
        cs += "-draw \"rectangle \"{}\",920 \"{}\",880\" ".format(
            asphaltlinks, asphaltlinks-50)
        cs += "-draw \"circle \"{}\",880 {},880\" ".format(
            asphaltlinks-20, asphaltlinks)
        cs += "-draw \"rectangle \"{}\",920 \"{}\",860\" ".format(
            asphaltrechts+20, asphaltrechts+50)
        cs += "-draw \"rectangle \"{}\",920 \"{}\",880\" ".format(
            asphaltrechts, asphaltrechts+50)
        cs += "-draw \"circle \"{}\",880 {},880\" ".format(
            asphaltrechts+20, asphaltrechts)

    # zum Debuggen... cs=$cs"-annotate +200+300 \"$fahrradlinks,$fahrradmitte,$fahrradrechts,$fahrradbreitepx\" "
    cs += "-draw \"image SrcOver {},{} {},{} '{}'\" ".format(
        fahrradlinks, fahrradoben, fahrradbreitepx, fahrradhoehepx, fahrradbild)
    if autoschwarz == 1:
        cs += "-draw \"image SrcOver {},{} {},{} '{}'\" ".format(
            autolinks, autooben, autobreitepx, autohoehepx, autobildschwarz)
    else:
        cs += "-draw \"image SrcOver {},{} {},{} '{}'\" ".format(
            autolinks, autooben, autobreitepx, autohoehepx, autobildrot)
        cs += "-stroke black -fill none -strokewidth 1 "
        cs += "-draw \"stroke-dasharray 3 5 path 'M {},900 M {},{}'\" ".format(
            linksrechts, linksrechts, autooben)

    cs += "-stroke black -fill none -strokewidth 1 "
    cs += "-draw \"stroke-dasharray 3 5 path 'M {},920 M {},1020'\" ".format(
        asphaltlinks, asphaltlinks)
    cs += "-draw \"stroke-dasharray 3 5 path 'M {},920 M {},1020'\" ".format(
        asphaltlinks, asphaltlinks)
    cs += "-draw \"stroke-dasharray 3 5 path 'M {},920 L {},1020'\" ".format(
        asphaltrechts, asphaltrechts)
    cs += "-draw \"stroke-dasharray 3 5 path 'M {},920 L {},1020'\" ".format(
        linksrechts, linksrechts)
    cs += "-draw \"stroke-dasharray 3 5 path 'M {},920 L {},1020'\" ".format(
        rechtslinks, rechtslinks)

    if mittelstreifen == 1:
        cs += "-draw \"stroke-dasharray 3 5 path 'M {},920 L {},1020'\" ".format(
            mitte, mitte)

    if stvo == 0:
        cs += "-draw \"stroke-dasharray 3 5 path 'M {},900 L {},970'\" ".format(
            fahrradmitte, fahrradmitte)
    else:
        cs += "-draw \"stroke-dasharray 3 5 path 'M {},{} L {},970'\" ".format(
            fahrradrechts, fahrradoben, fahrradrechts)

    cs += "-draw \"stroke-dasharray 3 5 path 'M {},970 L {},{}'\" ".format(
        fahrradlinks, fahrradlinks, fahrradoben)
    cs += "-draw \"stroke-dasharray 3 5 path 'M {},970 L {},{}'\" ".format(
        autorechts, autorechts, autooben)

    cs += "-draw \"stroke-dasharray 2 2 path 'M {},1020 L {},1020'\" ".format(
        asphaltlinks, asphaltrechts)

    if stvo == 0:
        cs += "-draw \"stroke-dasharray 2 2 path 'M {},970 L {},970'\" ".format(
            fahrradmitte, rechtslinks)
    else:
        cs += "-draw \"stroke-dasharray 2 2 path 'M {},970 L {},970'\" ".format(
            fahrradrechts, rechtslinks)

    cs += "-draw \"stroke-dasharray 2 2 path 'M {},970 L {},970'\" ".format(
        fahrradlinks, autorechts)
    cs += "-pointsize 18 -fill black -stroke none "
    # dringend ändern – Zentrieren funktioniert nicht wirklich
    if seitenstreifen > 12:
        cs += "-draw \"text {},1010 '{}'\" ".format(
            seitenstreifentextlinks, seitenstreifen)
        cs += "-draw \"text {},1010 '{}'\" ".format(
            seitenstreifentextrechts, seitenstreifen)

    cs += "-draw \"text {},1010 '{}'\" ".format(
        fahrstreifentextlinks, fahrstreifen)
    if mittelstreifen == 1:
        cs += "-draw \"text {},1010 '{}'\" ".format(
            fahrstreifentextrechts, fahrstreifen)

    cs += "-draw \"text {},960 '{}'\" ".format(
        fahrradabstandtext, fahrradabstand)

    if autoschwarz == 0:
        cs += "-fill red -stroke red "

    cs += "-draw \"text {},960 '{}'\" ".format(autoabstandtext, autoabstand)
    # bis hier

    cs += filename

    return cs


def main(argv):
    args, parser = parse_arguments(argv)

    if args.info:
        parser.print_help()
        print(infotext)
        sys.exit(0)

    command = create_convert_command(args)
    os.system(command)


def parse_arguments(argv):
    parser = argparse.ArgumentParser(
        description='Generiert ein Bild von einem Fahrrad, das überholt wird. Ansicht von hinten.')
    parser.add_argument("--fahrstreifen", help="Gemessen vom inneren Rand der äußeren Fahrstreifenbegrenzung bzw. vom Bordstein bis zur Mitte der Mittellinie. Ist keine Mittellinie vorhanden, bezeichnet dieser Wert die Breite der Straße zwischen den Bordsteinen bzw. den inneren Rändern der Fahrstreifenbegrenzungen. Defaultwert: 326 cm.¹", dest='fahrstreifen', action="store", type=int, default=326)
    parser.add_argument("--fahrradbreite", help="Die Gesamtbreite des Fahrrads von Lenkerende zu Lenkerende. Grafisch wird das Fahrrad-Bild einfach in x-Richtung verzerrt (ja, könnte man bestimmt schöner machen). Defaultwert: 65 cm.²",
                        dest='fahrradbreite', action="store", type=int, default=65)
    parser.add_argument("--typ", help="Da man schlecht mal eben nachmessen kann, wie breit das Auto eben war, gibt es hier drei standardisierte Fahrzeuge. 0 bezeichnet ein Auto. Laut Bußgeldstelle Pforzheim ist ein Auto ca. 180 cm breit³, daher wurde hier ein Mini Clubman angenommen. Der ist ohne Spiegel 180 cm, mit Spiegeln 203 cm breit⁴. 1 bezeichnet einen SUV. Als Beispiel habe ich einen Audi Q7 genommen: ohne Spiegel 197 cm, mit Spiegeln 2,22 m breit⁴. 2 bezeichnet einen LKW mit maximalen Abmessungen: 255 cm Fahrzeugbreite⁵, mit Spiegeln 300 cm⁶. Kühlfahrzeuge dürfen sogar 260 cm breit sein - hier nicht berücksichtigt. Alle drei Fahrzeuge haben die proportional richtige Höhe in der grafischen Darstellung. Defaultwert: 0 (Auto).", dest='typ', action="store", type=int, default=0)
    parser.add_argument("--mittelstreifen", help="0 bedeutet nein, 1 steht für ja. Wirkt sich auf die Behandlung des Parameters Fahrstreifenbreite aus - siehe dort. Defaultwert: 1 (mit Mittelstreifen).",
                        dest='mittelstreifen', action="store", type=int, default=1)
    parser.add_argument("--autoabstand", help="Wird gemessen vom Lenkerende bis zur Außenspiegelkante. Ist die Straße zu schmal, wird das überholende Fahrzeug so weit wie möglich nach links gesetzt (Außenspiegelkante auf Fahrstreifenbegrenzung), rot eingefärbt und der tatsächlich Abstand ausgegeben. Vorgeschriebene Abstände sind 200 cm außerorts und 150 cm innerorts. Handelt es sich bei den radfahrenden Personen um Kinder, ist auch innerorts der vorgeschriebene Überholabstand 200 cm. Defaultwert: 200 cm (wie außerorts vorgeschrieben).", dest='autoabstand', action="store", type=int, default=200)
    parser.add_argument("--stvo", help="Art der Bestimmung des Abstands des Fahrrads von der rechten Fahrstreifenbegrenzung³. Ein Gerichtsurteil schreibt hier 75 cm vor. Die Bußgeldstelle Pforzheim ist aber der Ansicht, dass die Fahrspur des Rads (also die Mitte der Reifen) 75 cm von der Fahrstreifenbegrenzung sein sollte. Beide Messmethoden sind einstellbar, nach StVO: 1, nach Pforzheimer Bußgeldstelle: 0. Defaultwert: 1 (nach StVO).", dest='stvo', action="store", type=int, default=1)
    parser.add_argument("--fahrradabstand", help="Die Art der Abstandsbestimmung wird in --stvo festgelegt - hier kommt der Wert. Defaultwert: 75 cm.",
                        dest='fahrradabstand', action="store", type=int, default=75)
    parser.add_argument("--seitenstreifen", help="Gemäß den Richtlinien für das Anlegen von Landstraßen⁷ endet die Asphaltdecke nicht direkt am Begrenzugsstrich, sondern etwas weiter außen. Die Breite dieses Seitenstreifens geht also vom Ende der Asphaltdecke bis zum inneren Rand der Begrenzungslinie. Ist dieser Wert 0, so wird direkt an die Fahrspur angrenzend ein Bordstein gezeichnet. (ja, man könnte noch einen Rinnstein vorsehen…) Defaultwert: 50 cm (entsprechend den aktuellen Richtlinien, bis Mai 2013: 25 cm).", dest='seitenstreifen', action="store", type=int, default=50)
    parser.add_argument("--mensch", help="Rein kosmetische Option. 0 wählt als Bild ein leeres Fahrrad, wie es auf offiziellen Verkehrszeichen⁸ zu sehen ist. 1 wählt eine radfahrende Person⁹. Defaultwert: 1.",
                        dest='mensch', action="store", type=int, default=1)
    parser.add_argument("--info", help="Zeigt die Hilfe und den Infotext (Quellenangaben) an und beendet sich.",
                        dest='info', action="store_true")
    parser.add_argument("--nourl", help="Unterdrückt die Anzeige des Git-Repositorys unten rechts",
                        dest='nourl', action="store_true")

    args = parser.parse_args(argv)
    return args, parser


if __name__ == "__main__":
    main(sys.argv[1:])
