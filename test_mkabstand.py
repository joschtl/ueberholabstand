# -*- coding: utf-8 -*-
from pathlib import Path

import mkabstand


def test_parse_arguments_without_arguments():
    args, parser = mkabstand.parse_arguments([])
    assert parser is not None
    assert args.fahrstreifen == 326
    assert args.fahrradbreite == 65
    assert args.typ == 0
    assert args.mittelstreifen == 1
    assert args.autoabstand == 200
    assert args.fahrradabstand == 75
    assert args.seitenstreifen == 50
    assert args.mensch == 1
    assert args.info == False
    assert args.nourl == False


def test_parse_arguments_with_typ_set_to_lkw():
    args, _ = mkabstand.parse_arguments(["--typ", "2"])
    assert args.typ == 2


def created_pngs(path='.'):
    return list(Path(path).glob('Abstand*.png'))


def test_calling_main_creates_png():
    try:
        pngs = created_pngs()
        assert len(pngs) == 0
        mkabstand.main([])
        pngs = created_pngs()
        assert len(pngs) == 1
        image = pngs[0]
        assert image.stat().st_size > 0
    finally:
        for f in created_pngs():
            f.unlink()
