PYTHON ?= python3
PIP ?= pip3
VENV_NAME ?= venv
COVERAGE_XML ?= cov.xml
TEST_XML ?= report.xml

.PHONY: all test clean

all: test

$(VENV_NAME): $(VENV_NAME)/bin/activate

$(VENV_NAME)/bin/activate: requirements.txt dev-requirements.txt
	test -d $(VENV_NAME) || $(PYTHON) -m venv $(VENV_NAME)
	. $(VENV_NAME)/bin/activate; $(PIP) install --upgrade --requirement requirements.txt --requirement dev-requirements.txt
	touch $(VENV_NAME)/bin/activate

test: $(VENV_NAME)
	. $(VENV_NAME)/bin/activate; pytest --cov mkabstand --cov-report xml:$(COVERAGE_XML) --junitxml=report.xml

clean:
	$(RM) -rf $(VENV_NAME) $(COVERAGE_XML) $(TEST_XML) __pycache__ .pytest_cache .coverage
	find -name "*.pyc" -delete
	find -name "Abstand*.png" -delete
