# Überholabstand

Erstellt eine Grafik vom Überholvorgang eines Fahrrads durch ein Kraftfahrzeug.

Benötigt Linux und ImageMagick (keine Ahnung, ob's mit MacOS auch geht, oder ob es unter Windows einen Workaround gibt).

Aufruf ohne Parameter gibt kurze Hilfestellung.

Das Ganze ist organisch gewachsen, nicht geplant. Die Liste der Dinge, die dringend überarbeitet werden müssen, enthält und ist nicht beschränkt auf folgende Punkte:
- das mangelhafte Zentrieren der Bemaßungszahlen (ImageMagick macht das wirklich unnötig schwer - und es funktioniert zudem nicht mal wie beschrieben)
- die inkonsistente Benennung von Variablen
- die monolithische Struktur des Ganzen
- unlogische Parameter
- Vektor- statt Pixelgrafik

Die Liste der hinzuzufügenden Dinge ist nur deshalb kürzer, weil ich bisher kaum darüber nachgedacht habe.
- geparkte Autos am Straßenrand
- Diversifizierung des Fahrrads (Lastenrad, Dreirad, Anhänger, …)
- mehrspurige Straßen. Nicht nur Fahrstreifen, sondern auch Radwege, Mordstreifen, Parkstreifen, Dooring-Zone-Schutzstreifen, …

